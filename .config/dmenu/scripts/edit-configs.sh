#!/bin/sh
#  ____ _____
# |  _ \_   _|  Derek Taylor (DistroTube)
# | | | || |    http://www.youtube.com/c/DistroTube
# | |_| || |    http://www.gitlab.com/dwt1/
# |____/ |_|
#
# Dmenu script for editing some of my more frequently edited config files.


declare options=("alacritty
awesome
bash
fish-config
fish-hotkeys
fish-theme
fish-theme-colour
lynx-cfg
lynx-lss
neovim
picom
vifm
vim
xresources
quit")

choice=$(echo -e "${options[@]}" | dmenu -i -p 'Edit config file: ')

case "$choice" in
	quit)
		echo "Program terminated." && exit 1
	;;
	alacritty)
		choice="$HOME/.config/alacritty/alacritty.yml"
	;;
	awesome)
		choice="$HOME/.config/awesome/rc.lua"
	;;
	bash)
		choice="$HOME/.bash_aliases"
	;;
    fish-config)
		choice="$HOME/.config/fish/config.fish"
	;;
    fish-hotkeys)
		choice="$HOME/.config/fish/functions/fish_user_key_bindings.fish"
	;;
    fish-theme)
		choice="$HOME/.config/fish/functions/fish_prompt.fish"
	;;
    fish-theme-colour)
		choice="$HOME/.local/share/omf/themes/bobthefish/functions/__bobthefish_colors.fish"
	;;
	lynx-cfg)
		choice="$HOME/.config/lynx/lync.cfg"
	;;
	lynx-lss)
		choice="$HOME/.config/lynx/lync.lss"
	;;
	neovim)
		choice="$HOME/.config/nvim/init.vim"
	;;
	picom)
		choice="$HOME/.config/picom/picom.conf"
	;;
	vifm)
		choice="$HOME/.config/vifm/vifmrc"
	;;
	vim)
		choice="$HOME/.vimrc"
	;;
	xresources)
		choice="$HOME/.Xresources"
	;;
	*)
		exit 1
	;;
esac
alacritty -e nvim "$choice"

