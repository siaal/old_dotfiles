# Defined in - @ line 1
function lg --wraps='ll | grep' --description 'alias lg=ll | grep'
  ll | grep $argv;
end
