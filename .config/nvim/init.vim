set nocompatible              " be iMproved, required
filetype off                  " required

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Vundle For Managing Plugins
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim

call vundle#begin()		" required, all plugins must appear after this line.
"{{ The Basics }}
    Plugin 'gmarik/Vundle.vim'                           " Vundle
    Plugin 'itchyny/lightline.vim'                       " Lightline statusbar
    Plugin 'suan/vim-instant-markdown', {'rtp': 'after'} " Markdown Preview
    Plugin 'andymass/vim-matchup'                        " Expanding the % command to words and quotes
    Plugin 'mbbill/undotree'                             " tree of undoing
    Plugin 'tpope/vim-speeddating'                       " increment dates, times and potentially more using the <C-a><C-x> commands
"{{ Expanding Vim's Language }}
    Plugin 'tpope/vim-repeat'                            " . repeat for plugins
    Plugin 'tpope/vim-surround'                          " adds the 's'urroundings motion to be 'd'eleted or 'c'hanged or 'y'dded { adds spaces that } does not yss applies to all lines because double s
    Plugin 'tpope/vim-commentary'                        " adds 'gc' operator to comment. supports gcc
    Plugin 'christoomey/vim-sort-motion'                 " sorting as an operator
    Plugin 'christoomey/vim-titlecase'                   " title casing
    Plugin 'vim-scripts/ReplaceWithRegister'             " adds a 'gr' operator to replace from register
    Plugin 'tpope/vim-unimpaired'                        " adds...a lot of good bindings
"{{ More text objects }}
    Plugin 'kana/vim-textobj-user'                       " enables custom textobj
    " Plugin 'kana/vim-textobj-entire'                     " adds a entire and inner entire text object, disabled for safety.
    Plugin 'kana/vim-textobj-indent'                     " adds inner indent and a indent text object
    Plugin 'kana/vim-textobj-line'                       " inner line and a line text objects
"{{ File management }}
    Plugin 'vifm/vifm.vim'                               " Vifm
    Plugin 'scrooloose/nerdtree'                         " Nerdtree
    Plugin 'tiagofumo/vim-nerdtree-syntax-highlight'     " Highlighting Nerdtree
    Plugin 'ryanoasis/vim-devicons'                      " Icons for Nerdtree
"{{ Productivity }}
"    Plugin 'vimwiki/vimwiki'                             " VimWiki 
    " Plugin 'airblade/vim-gitgutter'                      " shows git interaction in sign column
"{{ Syntax Highlighting and Colors }}
    Plugin 'vim-syntastic/syntastic'                     " syntax error checking on write
    Plugin 'vim-pandoc/vim-pandoc'                       " Pandoc functionality
    Plugin 'https://gitlab.com/rwxrob/vim-pandoc-syntax-simple' "rob's pandoc syntax
    Plugin 'cespare/vim-toml'                            " toml highlighting
    Plugin 'vim-python/python-syntax'                    " Python highlighting
    Plugin 'ap/vim-css-color'                            " Color previews for CSS
    Plugin 'fatih/vim-go'                                " go highlighting
    " Plugin 'norcalli/nvim-colorizer.lua'                 " color preview for *neovim only*
    "Plugin 'skammer/vim-css-color'        never use, causes MD to load slowly
"{{ Autocomplete}}
    Plugin 'alvan/vim-closetag'                          " automatically generated </closing html tags>
call vundle#end()		" required, all plugins must appear before this line.

filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on

" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal

" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Remap Keys
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"Remap ESC to kj
:imap kj <Esc>

" Ctrl + S save
:nnoremap <C-s> :w<CR> 

" Since Ctrl-V is save, Ctrl B is visual block
:nnoremap <C-b> <C-v>

"Since Ctrl-B is visual block, Ctrl-Z is page up
:nnoremap <C-z> <C-b>

"Function making funtion, |{
inoremap \|{ {<CR><CR>}<Esc>ki<Tab>

" disable arrow keys (vi muscle memory)
noremap <up> :echoerr "Umm, use k instead"<CR>
noremap <down> :echoerr "Umm, use j instead"<CR>
noremap <left> :echoerr "Umm, use h instead"<CR>
noremap <right> :echoerr "Umm, use l instead"<CR>
inoremap <up> <NOP>
inoremap <down> <NOP>
inoremap <left> <NOP>
inoremap <right> <NOP>

" Making new lines without entering insert mode
nnoremap <S-Enter> O<Esc>
nnoremap <C-Enter> o<Esc>

" functions keys
map <F1> :set relativenumber!<CR>
map <F2> :set termguicolors!<CR>
set pastetoggle=<F3>
map <F4> :set list!<CR>
map <F5> :set cursorline!<CR>
map <F6> :set incsearch!<CR>
map <F7> :set spell!<CR>
map <F8> :UndotreeToggle<CR>
map <F9> :set scrolloff=1 cursorline<CR>
map <F10> :set scrolloff=999 nocursorline<CR>
map <F11> :set wrapscan!<CR>
map <F12> :set fdm=indent<CR>

inoremap \|html <esc>:-1read $HOME/.vim/bases/skeleton.html<CR>3jwf>a

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""" 
" => General Settings 
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set directory^=$HOME/.vim/tmp/swap// " directory to store swapfiles
set backupdir=$HOME/.vim/tmp/backup// " directory to store swapfiles
set backup                      " Backups, but only in the fancy tmp directory
set swapfile                    " Always have a swapfile. Always
set path+=**					" Searches current directory recursively. 
set wildmenu					" Display all matches when tab complete. 
set hidden                      " Needed to keep multiple buffers open 
set ruler                       " enables ruler
set t_Co=256                    " Set if term supports 256 colors. 
" set termguicolors             " introduce more colours and completely dick my colour scheme
set number relativenumber       " Display line numbers 
set clipboard=unnamedplus       " Copy/paste between vim and other programs. 
set ttyfast                     " faster scrolling
syntax enable                   " needed for plugins
set complete+=i                 " autocomplete from included libraries
set complete+=kspell            " autocomplete from dictionary, but only if spellcheck is enabled
let g:rehash256 = 1
set textwidth=0
" set textwidth=73
set foldmethod=manual           " disable auto folds
set cursorline                  " underlines current row (can be changed in hi cursorline)
set omnifunc=syntaxcomplete#Complete " enable omni-completion 
set nrformats-=octal            " remove base 8 from increment because who actuall uses base 8
set mouse=nicr                  " Mouse scrolling and functionality inside normal insert command and replace mode

set nostartofline
set linebreak

" start at last place you were editing
au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif


" easier to see characters when `set paste` is on 
set listchars=tab:→\ ,eol:↲,nbsp:␣,space:·,trail:·,extends:⟩,precedes:⟨



"set scrolloff=999     " center the cursor always on the screen
set scrolloff=1        " always ahve at least one more line below the cursor

" Format Options
set formatoptions-=t   " don't auto-wrap text using text width
"set formatoptions+=c   " autowrap comments using textwidth with leader
"set formatoptions-=r   " don't auto-insert comment leader on enter in insert
"set formatoptions-=o   " don't auto-insert comment leader on o/O in normal
set formatoptions+=q   " allow formatting of comments with gq
set formatoptions-=w   " don't use trailing whitespace for paragraphs
set formatoptions-=a   " disable auto-formatting of paragraph changes
set formatoptions-=n   " don't recognized numbered lists
set formatoptions+=j   " delete comment prefix when joining
set formatoptions-=2   " don't use the indent of second paragraph line
set formatoptions-=v   " don't use broken 'vi-compatible auto-wrapping'
set formatoptions-=b   " don't use broken 'vi-compatible auto-wrapping'
set formatoptions+=l   " long lines not broken in insert mode
set formatoptions+=m   " multi-byte character line break support
set formatoptions+=M   " don't add space before or after multi-byte char
set formatoptions-=B   " don't add space between two multi-byte chars in join 
set formatoptions+=1   " don't break a line after a one-letter word
au FileType * set fo+=c fo-=r fo-=o " otherwise filetypeplugin overwrites them

" Text, tab and indent related 
set expandtab                   " Use spaces instead of tabs. 
set smarttab                    " Be smart using tabs ;) 
set autoindent
set shiftwidth=4                " One tab == four spaces. 
set tabstop=4                   " One tab == four spaces.
set softtabstop=4
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Pandoc
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" let g:pandoc#spell#enabled=0

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"" => GitGutter
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
":hi gitgutteradd ctermbg=none ctermfg=7
":hi gitgutterchange ctermbg=none ctermfg=7
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Powerline
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:lightline = {
      \ 'colorscheme': 'darcula',
      \ }

" Always show statusline
set laststatus=2

" Uncomment to prevent non-normal modes showing in powerline and below powerline.
set noshowmode


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Force some file names to be specific file type
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"au bufnewfile,bufRead *.bash* set ft=sh
au bufnewfile,bufRead *.profile set filetype=sh
au bufnewfile,bufRead *.crontab set filetype=crontab
au bufnewfile,bufRead *ssh/config set filetype=sshconfig
au bufnewfile,bufRead *gitconfig set filetype=gitconfig
au bufnewfile,bufRead /tmp/psql.edit.* set syntax=sql

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => NERDTree
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Uncomment to autostart the NERDTree
" autocmd vimenter * NERDTree
map <C-\> :NERDTreeToggle<CR>
let g:NERDTreeDirArrowExpandable = '►'
let g:NERDTreeDirArrowCollapsible = '▼'
let NERDTreeShowLineNumbers=1
let NERDTreeShowHidden=1
let NERDTreeMinimalUI = 1
let g:NERDTreeWinSize=38

" NERDTress File highlighting
function! NERDTreeHighlightFile(extension, fg, bg, guifg, guibg)
 exec 'autocmd filetype nerdtree highlight ' . a:extension .' ctermbg='. a:bg .' ctermfg='. a:fg .' guibg='. a:guibg .' guifg='. a:guifg
 exec 'autocmd filetype nerdtree syn match ' . a:extension .' #^\s\+.*'. a:extension .'$#'
endfunction

call NERDTreeHighlightFile('jade', 'green', 'none', 'green', '#151515')
call NERDTreeHighlightFile('ini', 'yellow', 'none', 'yellow', '#151515')
call NERDTreeHighlightFile('md', 'blue', 'none', '#3366FF', '#151515')
call NERDTreeHighlightFile('yml', 'yellow', 'none', 'yellow', '#151515')
call NERDTreeHighlightFile('config', 'yellow', 'none', 'yellow', '#151515')
call NERDTreeHighlightFile('conf', 'yellow', 'none', 'yellow', '#151515')
call NERDTreeHighlightFile('json', 'yellow', 'none', 'yellow', '#151515')
call NERDTreeHighlightFile('html', 'yellow', 'none', 'yellow', '#151515')
call NERDTreeHighlightFile('styl', 'cyan', 'none', 'cyan', '#151515')
call NERDTreeHighlightFile('css', 'cyan', 'none', 'cyan', '#151515')
call NERDTreeHighlightFile('coffee', 'Red', 'none', 'red', '#151515')
call NERDTreeHighlightFile('js', 'Red', 'none', '#ffa500', '#151515')
call NERDTreeHighlightFile('php', 'Magenta', 'none', '#ff00ff', '#151515')

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Colors and Themeing
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" https://jonasjacek.github.io/colors/ for color codes
  highlight signcolumn ctermbg=none 
  highlight foldcolumn ctermbg=none ctermfg=8
  highlight Search           ctermfg=0  ctermbg=magenta       cterm=NONE
  highlight MatchParen ctermfg=magenta ctermbg=none cterm=bold " matching brackets coloring
  highlight pandocnewline ctermbg=blue cterm=underline
" :hi cursorline guibg=none gui=underline
" hi title cterm=bold ctermbg=none ctermfg=brown " work in progress
  " highlight LineNr           ctermfg=8    ctermbg=none    cterm=none
  " highlight CursorLineNr     ctermfg=7    ctermbg=8       cterm=none
  highlight LineNr           ctermfg=blue ctermbg=none         cterm=none
  highlight CursorLineNr     ctermfg=cyan ctermbg=8            cterm=none
  highlight VertSplit        ctermfg=0         ctermbg=8            cterm=none
  highlight Statement        ctermfg=2         ctermbg=none         cterm=none
  highlight Directory        ctermfg=4         ctermbg=none         cterm=none
  highlight StatusLine       ctermfg=7         ctermbg=8            cterm=none
  highlight StatusLineNC     ctermfg=7         ctermbg=8            cterm=none
  highlight NERDTreeClosable ctermfg=2     
  highlight NERDTreeOpenable ctermfg=8     
  highlight Comment          ctermfg=4         ctermbg=none         cterm=none
  highlight Constant         ctermfg=12        ctermbg=none         cterm=none
  highlight Special          ctermfg=4         ctermbg=none         cterm=none
  highlight Identifier       ctermfg=6         ctermbg=none         cterm=none
  highlight PreProc          ctermfg=5         ctermbg=none         cterm=none
  highlight String           ctermfg=12        ctermbg=none         cterm=none
  highlight Number           ctermfg=1         ctermbg=none         cterm=none
  highlight Function         ctermfg=1         ctermbg=none         cterm=none
  " highlight WildMenu         ctermfg=0         ctermbg=80           cterm=none
  " highlight Folded           ctermfg=103       ctermbg=234          cterm=none
  " highlight FoldColumn       ctermfg=103       ctermbg=234          cterm=none
  " highlight DiffAdd          ctermfg=none      ctermbg=23           cterm=none
  " highlight DiffChange       ctermfg=none      ctermbg=56           cterm=none
  " highlight DiffDelete       ctermfg=168       ctermbg=96           cterm=none
  " highlight DiffText         ctermfg=0         ctermbg=80           cterm=none
  " highlight SignColumn       ctermfg=244       ctermbg=235          cterm=none
  " highlight Conceal          ctermfg=251       ctermbg=none         cterm=none
  highlight SpellBad         ctermfg=168       ctermbg=8         cterm=underline
  highlight SpellCap         ctermfg=80        ctermbg=none         cterm=underline
  highlight SpellRare        ctermfg=black
  "" highlight SpellRare        ctermfg=121       ctermbg=none         cterm=underline
  highlight SpellLocal       ctermfg=186       ctermbg=none         cterm=underline
  " highlight Pmenu            ctermfg=251       ctermbg=234          cterm=none
  " highlight PmenuSel         ctermfg=0         ctermbg=111          cterm=none
  " highlight PmenuSbar        ctermfg=206       ctermbg=235          cterm=none
  " highlight PmenuThumb       ctermfg=235       ctermbg=206          cterm=none
  " highlight TabLine          ctermfg=244       ctermbg=234          cterm=none
  " highlight TablineSel       ctermfg=0         ctermbg=247          cterm=none
  " highlight TablineFill      ctermfg=244       ctermbg=234          cterm=none
  " highlight CursorColumn     ctermfg=none      ctermbg=236          cterm=none
  " highlight CursorLine       ctermfg=none      ctermbg=236          cterm=none
  " highlight ColorColumn      ctermfg=none      ctermbg=236          cterm=none
  " highlight Cursor           ctermfg=0         ctermbg=5            cterm=none
  " highlight htmlEndTag       ctermfg=114       ctermbg=none         cterm=none
  " highlight xmlEndTag        ctermfg=114       ctermbg=none         cterm=none


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Vifm
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
map <Leader>vv :Vifm<CR>
map <Leader>vs :VsplitVifm<CR>
map <Leader>sp :SplitVifm<CR>
map <Leader>dv :DiffVifm<CR>
map <Leader>tv :TabVifm<CR>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""" 
" => Vim-Instant-Markdown 
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""" 
let g:instant_markdown_autostart = 0         " Turns off auto preview 
let g:instant_markdown_browser = 'firefox --new-window'          " Uses lynx for preview 
map <Leader>md :InstantMarkdownPreview<CR>   " Previews .md file 
map <Leader>ms :InstantMarkdownStop<CR>      " Kills the preview 

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""" 
" => Open terminal inside Vim 
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""" 
map <Leader>tt :vnew term://fish<CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Search Settings
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
:set ignorecase " ignore search case
:set smartcase  " become case sensitivity if contains a capital
:set hlsearch          " search highlights words
:set noincsearch  " will not search until enter is pressed
" Press Space to turn off highlighting and clear any message already displayed.
:nnoremap <silent> <Space> :nohlsearch<Bar>:echo<CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Splits and Tabbed Files
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set splitbelow splitright

" Remap splits navigation to just CTRL + hjkl 
nnoremap <C-h> <C-w>h 
nnoremap <C-j> <C-w>j 
nnoremap <C-k> <C-w>k 
nnoremap <C-l> <C-w>l

" Make adjusing split sizes a bit more friendly 
noremap <silent> <C-Left> :vertical resize +3<CR> 
noremap <silent> <C-Right> :vertical resize -3<CR> 
noremap <silent> <C-Up> :resize +3<CR> 
noremap <silent> <C-Down> :resize -3<CR>

" Change 2 split windows from vert to horiz or horiz to vert 
map <Leader>th <C-w>t<C-w>H 
map <Leader>tk <C-w>t<C-w>K 

" Removes pipes | that act as seperators on splits 
set fillchars+=vert:\ 


let g:minimap_highlight='Visual'

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Removes pipes | that act as seperators on splits
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set fillchars+=vert:\ 
