local theme_assets = require("beautiful.theme_assets")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi
--local themes_path = require("gears.filesystem").get_themes_dir()
themes_path = "~/.config/awesome/themes/siaal/"
theme = {}
--[[
theme.font              = "Noto Sans Regular 10"
theme.notification_font = "Noto Sans Bold 14"

theme.bg_normal   = "#141A1B"
theme.bg_focus    = "#222B2E"
theme.bg_urgent   = "#000000"
theme.bg_minimize = "#101010"
theme.bg_systray  = theme.bg_normal

theme.fg_normal   = "#ffffff"
theme.fg_focus    = "#ffffff"
theme.fg_urgent   = "#ff0000"
theme.fg_minimize = "#ffffff"

theme.border_width  = 1
theme.border_normal = "#000000"
theme.border_focus  = "#16A085"
theme.border_marked = "#16A085"
--]]

local background   = "#282a36"
local selection    = "#44475a"
local foreground   = "#f8f8f2"
local comment      = "#6272a4"
local cyan         = "#8be9fd"
local green        = "#50fa7b"
local orange       = "#ffb86c"
local pink 	       = "#ff79c6"
local purple 	   = "#bd93f9"
local red 	       = "#ff5555"
local yellow 	   = "#f1fa8c"


  -- # Normal colors
-- local black=   '#282a36'
-- local red=     '#f07178'
-- local green=   '#c3e88d'
-- local yellow=  '#ffcb6b'
-- local blue=    '#82aaff'
-- local magenta= '#c792ea'
-- local cyan=    '#89ddff'
-- local white=   '#d0d0d0'
-- local purple=  '#bd93f9'

  -- # Bright colors
-- local black=   '#434758'
-- local red=     '#ff8b92'
-- local green=   '#ddffa7'
-- local yellow=  '#ffe585'
-- local blue=    '#9cc4ff'
-- local magenta= '#e1acff'
-- local cyan=    '#a3f7ff'
-- local white=   '#ffffff'
-- local purple=  '#bd93f9'

--theme.font          ="sans 10"
theme.font        ="Noto Sans Regular 10"
--theme.font          = "Noto Sans Mono Medium 10"
theme.notification_font = "Noto Sans Bold 10"

theme.bg_normal     = background
theme.bg_focus      = selection
theme.bg_minimize   = background
theme.bg_systray    = theme.bg_normal

theme.bg_urgent     = comment
--theme.bg_urgent = "#bd93f9"
theme.fg_urgent = "#ffffff"

theme.fg_normal     = foreground
theme.fg_focus      = theme.fg_normal
theme.fg_minimize   = comment

theme.useless_gap   = dpi(0)
theme.border_width  = dpi(2)
theme.border_normal = "#000000"
theme.border_focus  = selection
theme.border_marked = background

theme.hotkeys_modifiers_fg = comment -- in the hotkeys menu, color of mod keys

-- theme.bg_normal     = "#222222"
-- theme.bg_focus      = "#535d6c"
-- theme.bg_minimize   = "#444444"
-- theme.bg_systray    = theme.bg_normal
-- 
-- --theme.bg_urgent     = "#6272a4"--"#ff0000"
-- theme.bg_urgent = "#bd93f9"
-- theme.fg_urgent = "#000000"
-- --theme.fg_urgent     = "#ffffff"
-- 
-- theme.fg_normal     = "#f8f8f2"
-- theme.fg_focus      = "#f8f8f2"
-- theme.fg_minimize   = "#ffffff"
-- 



-- There are other variable sets
-- overriding the default one when
-- defined, the sets are:
-- [taglist|tasklist]_[bg|fg]_[focus|urgent]
-- titlebar_[bg|fg]_[normal|focus]
-- tooltip_[font|opacity|fg_color|bg_color|border_width|border_color]
-- mouse_finder_[color|timeout|animate_timeout|radius|factor]
-- Example:
--theme.taglist_bg_focus = "#ff0000"

-- Generate the taglist squares
local taglist_square_size = dpi(4)
theme.taglist_squares_sel = theme_assets.taglist_squares_sel(
    taglist_square_size, theme.fg_normal
)
theme.taglist_squares_unsel = theme_assets.taglist_squares_unsel(
    taglist_square_size, theme.fg_normal
)

-- Variables set for theming the menu:
-- menu_[bg|fg]_[normal|focus]
-- menu_[border_color|border_width]
theme.menu_submenu_icon = themes_path .. "submenu.png"
--theme.menu_height = 25
--theme.menu_width  = 200
theme.menu_height = dpi(15)
theme.menu_width = dpi(100)

-- You can add as many variables as
-- you wish and access them by using
-- beautiful.variable in your rc.lua
--theme.bg_widget = "#cc0000"

-- Define the image to load
theme.titlebar_close_button_normal              = themes_path .. "titlebar/close_normal_arc.png"
theme.titlebar_close_button_focus               = themes_path .. "titlebar/close_focus_arc.png"

theme.titlebar_ontop_button_normal_inactive     = themes_path .. "titlebar/ontop_normal_inactive.png"
theme.titlebar_ontop_button_focus_inactive      = themes_path .. "titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_active       = themes_path .. "titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_active        = themes_path .. "titlebar/ontop_focus_active.png"

theme.titlebar_sticky_button_normal_inactive    = themes_path .. "titlebar/sticky_normal_inactive.png"
theme.titlebar_sticky_button_focus_inactive     = themes_path .. "titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_active      = themes_path .. "titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_active       = themes_path .. "titlebar/sticky_focus_active.png"

theme.titlebar_floating_button_normal_inactive  = themes_path .. "titlebar/floating_normal_inactive.png"
theme.titlebar_floating_button_focus_inactive   = themes_path .. "titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_active    = themes_path .. "titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_active     = themes_path .. "titlebar/floating_focus_active.png"

theme.titlebar_maximized_button_normal_inactive = themes_path .. "titlebar/maximized_normal_inactive.png"
theme.titlebar_maximized_button_focus_inactive  = themes_path .. "titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_active   = themes_path .. "titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_active    = themes_path .. "titlebar/maximized_focus_active.png"

theme.wallpaper = themes_path .. "awesome-scrabble.png"

-- You can use your own layout icons like this:
theme.layout_fairh      = themes_path .. "layouts/fairh.png"
theme.layout_fairv      = themes_path .. "layouts/fairv.png"
theme.layout_floating   = themes_path .. "layouts/floating.png"
theme.layout_magnifier  = themes_path .. "layouts/magnifier.png"
theme.layout_max        = themes_path .. "layouts/max.png"
theme.layout_fullscreen = themes_path .. "layouts/fullscreen.png"
theme.layout_tilebottom = themes_path .. "layouts/tilebottom.png"
theme.layout_tileleft   = themes_path .. "layouts/tileleft.png"
theme.layout_tile       = themes_path .. "layouts/tile.png"
theme.layout_tiletop    = themes_path .. "layouts/tiletop.png"
theme.layout_spiral     = themes_path .. "layouts/spiral.png"
theme.layout_dwindle    = themes_path .. "layouts/dwindle.png"
theme.layout_cornernw   = themes_path .. "layouts/cornernw.png"
theme.layout_cornerne   = themes_path .. "layouts/cornerne.png"
theme.layout_cornersw   = themes_path .. "layouts/cornersw.png"
theme.layout_cornerse   = themes_path .. "layouts/cornerse.png"
--theme.awesome_icon = themes_path .. "icons/manjaro64.png"

-- Define the icon theme for application icons. If not set then the icons 
-- from /usr/share/icons and /usr/share/icons/hicolor will be used.
--theme.icon_theme = "Papirus-Dark"

return theme
