# Defined in - @ line 1
function rm --wraps='rm -I --preserve-root' --description 'alias rm=rm -I --preserve-root'
 command rm -I --preserve-root $argv;
end
