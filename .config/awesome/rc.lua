-- If LuaRocks is installed, make sure that packages installed through it are
-- found (e.g. lgi). If LuaRocks is not installed, do nothing.
pcall(require, "luarocks.loader")
-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")
-- Widget and layout library
local wibox = require("wibox")
local ruled = require("ruled")
-- Theme handling library
local beautiful = require("beautiful")
-- Notification library
local naughty = require("naughty")
local menubar = require("menubar")
local hotkeys_popup = require("awful.hotkeys_popup").widget
-- Freedesktop menu
local freedesktop = require("freedesktop")
local lain    = require("lain")
local my_table      = awful.util.table or gears.table
--local ruled = require("ruled.notifications")
-- Enable VIM help for hotkeys widget when client with matching name is opened:
require("awful.hotkeys_popup.keys.vim")

-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
    naughty.notify({ preset = naughty.config.presets.critical,
                                  title = "Oops, there were errors during startup!",
                                  text = awesome.startup_errors })
end
-- this is a comment
-- Handle runtime errors after startup
do
    local in_error = false
    awesome.connect_signal("debug::error", function (err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true
        naughty.notify({ preset = naughty.config.presets.critical,
                                      title = "Oops, an error happened!",
                                      text = tostring(err) })
        in_error = false
    end)
end
-- }}}

-- {{{ Variable definitions
-- Themes define colours, icons, font and wallpapers.
-- Chosen colors and buttons look alike adapta maia theme
--beautiful.init("/usr/share/awesome/themes/cesious/theme.lua")
theme_path = "~/.config/awesome/themes/"
beautiful.init(theme_path.. "siaal/theme.lua")

--beautiful.icon_theme        = ""
--beautiful.bg_normal         = "#FFFFFF"
--beautiful.bg_focus          = "#2C3940"
beautiful.titlebar_close_button_normal = "/usr/share/awesome/themes/cesious/titlebar/close_normal_adapta.png"
beautiful.titlebar_close_button_focus = "/usr/share/awesome/themes/cesious/titlebar/close_focus_adapta.png"
--beautiful.font              = "Noto Sans Regular 10"
--beautiful.notification_font = "Noto Sans Bold 10"

-- This is used later as the default terminal and editor to run.
terminal                = "alacritty" or os.getenv("TERMINAL") --"terminator"
browser                 = "firefox" or "exo-open --launch WebBrowser"
filemanager             = "nautilus -w" or "exo-open --launch FileManager"
editor                  = terminal.. " -e nvim"
gui_editor              = "kate"
terminal_fish           = terminal .. " -e fish"
--lock                   = "i3lock -fei ../../Pictures/windows_desktop.png -c 000000"
--lock                   = "xautolock -locknow"
--lock                    = "dm-tool lock"
lock                    ="xscreensaver-command -lock"
music_player            ="audacious"
next_song               ="audtool playlist-advance"
previous_song           ="audtool playlist-reverse"
toggle_pause            ="audtool playback-playpause"
calculator              = "galculator"
--backup_browser          = "vivaldi"
backup_browser          = "chromium"
wibar_pos               = "bottom"
--wibar_pos               = "top" 
task_manager            = terminal.. "-e htop"
--task_manager             = "Xfce4-taskmanager"

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
modkey = "Mod4"
local altkey       = "Mod1"
-- Table of layouts to cover with awful.layout.inc, order matters.
awful.layout.layouts = {
    awful.layout.suit.tile,
    awful.layout.suit.tile.bottom,
    awful.layout.suit.magnifier,	
    lain.layout.cascade.tile,
    awful.layout.suit.tile.left
    --awful.layout.suit.floating -- just use mod+Y
--[[
    awful.layout.suit.tile,
    awful.layout.suit.floating,
    awful.layout.suit.tile.left,
    awful.layout.suit.tile.bottom,
    awful.layout.suit.tile.top,
    awful.layout.suit.fair,
    awful.layout.suit.fair.horizontal,
    awful.layout.suit.spiral,
    awful.layout.suit.spiral.dwindle,
    awful.layout.suit.max,
    awful.layout.suit.max.fullscreen,
    awful.layout.suit.magnifier,
    awful.layout.suit.corner.nw,
    awful.layout.suit.corner.ne,
    awful.layout.suit.corner.sw,
    awful.layout.suit.corner.se,
    lain.layout.cascade,
    lain.layout.cascade.tile,
    lain.layout.centerwork,
    lain.layout.centerwork.horizontal,
    lain.layout.termfair,
    lain.layout.termfair.center
--]]
}
-- }}}

-- {{{ Helper functions
local function client_menu_toggle_fn()
    local instance = nil
    return function ()
        if instance and instance.wibox.visible then
            instance:hide()
            instance = nil
        else
            instance = awful.menu.clients({ theme = { width = 250 } })
        end
    end
end
-- }}}


-- {{{ Menu
-- Create a launcher widget and a main menu
myawesomemenu = {
    { "hotkeys", function() return false, hotkeys_popup.show_help end, menubar.utils.lookup_icon("preferences-desktop-keyboard-shortcuts") },
    { "manual", terminal .. " -e man awesome", menubar.utils.lookup_icon("system-help") },
    { "edit config", gui_editor .. " " .. awesome.conffile,  menubar.utils.lookup_icon("accessories-text-editor") },
    { "restart", awesome.restart, menubar.utils.lookup_icon("system-restart") }
}
myexitmenu = {
    { "log out", function() awesome.quit() end, menubar.utils.lookup_icon("system-log-out") },
    { "suspend", "systemctl suspend", menubar.utils.lookup_icon("system-suspend") },
    { "hibernate", "systemctl hibernate", menubar.utils.lookup_icon("system-suspend-hibernate") },
    { "reboot", "systemctl reboot", menubar.utils.lookup_icon("system-reboot") },
    { "shutdown", "poweroff", menubar.utils.lookup_icon("system-shutdown") }
}
mymainmenu = freedesktop.menu.build({
    icon_size = 32,
    before = {
        { "Terminal", terminal_fish, menubar.utils.lookup_icon("utilities-terminal") },
        { "Browser", browser, menubar.utils.lookup_icon("internet-web-browser") },
        { "Files", filemanager, menubar.utils.lookup_icon("system-file-manager") },
        -- other triads can be put here
    },
    after = {
        { "Awesome", myawesomemenu, "/usr/share/awesome/icons/awesome32.png" },
        { "Exit", myexitmenu, menubar.utils.lookup_icon("system-shutdown") },
        -- other triads can be put here
    }
})
mylauncher = awful.widget.launcher({ image = beautiful.awesome_icon,
                                     menu = mymainmenu })
-- Menubar configuration
menubar.utils.terminal = terminal_fish -- Set the terminal for applications that require it
-- }}}

-- {{{ Wibar
-- Create a textclock widget
mytextclock = wibox.widget.textclock('<span> %d.%m.%y   %I:%M %p   </span>')
-- Keyboard map indicator and switcher
mykeyboardlayout = awful.widget.keyboardlayout()

darkblue    = beautiful.bg_focus
blue        = "#9EBABA"
red         = "#EB8F8F"
white       = "#FFFFFF"
separator = wibox.widget.textbox(' <span color="' .. white .. '">|</span>')
spacer = wibox.widget.textbox(' <span color="' .. blue .. '"> </span>')

-- Create a wibox for each screen and add it
local taglist_buttons = gears.table.join(
                    awful.button({ }, 1, function(t) t:view_only() end),
                    awful.button({ modkey }, 1, function(t)
                                              if client.focus then
                                                  client.focus:move_to_tag(t)
                                              end
                                          end),
                    awful.button({ }, 3, awful.tag.viewtoggle),
                    awful.button({ modkey }, 3, function(t)
                                              if client.focus then
                                                  client.focus:toggle_tag(t)
                                              end
                                          end),
                    awful.button({ }, 4, function(t) awful.tag.viewprev(t.screen) end), --scroll on tag list
                    awful.button({ }, 5, function(t) awful.tag.viewnext(t.screen) end)
                )

local tasklist_buttons = gears.table.join(
                     awful.button({ }, 1, function (c)
                                              if c == client.focus then
                                                  c.minimized = true
                                              else
                                                  -- Without this, the following
                                                  -- :isvisible() makes no sense
                                                  c.minimized = false
                                                  if not c:isvisible() and c.first_tag then
                                                      c.first_tag:view_only()
                                                  end
                                                  -- This will also un-minimize
                                                  -- the client, if needed
                                                  client.focus = c
                                                  c:raise()
                                              end
                                          end),
                     awful.button({ }, 3, client_menu_toggle_fn()),
                     awful.button({ }, 4, function ()
                                              awful.client.focus.byidx(-1) --scroll on task list
                                          end),
                     awful.button({ }, 5, function ()
                                              awful.client.focus.byidx( 1)
                                          end))

local function set_wallpaper(s)
    -- Wallpaper
    if beautiful.wallpaper then
        local wallpaper = beautiful.wallpaper
        -- If wallpaper is a function, call it with the screen
        if type(wallpaper) == "function" then
            wallpaper = wallpaper(s)
        end
        gears.wallpaper.maximized(wallpaper, s, true)
    end
end

-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
--screen.connect_signal("property::geometry", set_wallpaper)

awful.screen.connect_for_each_screen(function(s)
    -- Wallpaper
    --set_wallpaper(s)

    -- Each screen has its own tag table.
    awful.tag({ " WWW ", " GAM ", " SYS ", " CHAT ", " DEV ", " VID ", " MUS ", " EXP ", " ETC " }, s, awful.layout.layouts[1])

    -- Create a promptbox for each screen
    s.mypromptbox = awful.widget.prompt()
    -- Create an imagebox widget which will contains an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(gears.table.join(
                           awful.button({ }, 1, function () awful.layout.inc( 1) end),
                           awful.button({ }, 3, function () awful.layout.inc(-1) end),
                           awful.button({ }, 4, function () awful.layout.inc( 1) end),
                           awful.button({ }, 5, function () awful.layout.inc(-1) end)))
    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist(s, awful.widget.taglist.filter.all, taglist_buttons)
	
    -- Create a tasklist widget
    s.mytasklist = awful.widget.tasklist(s, awful.widget.tasklist.filter.currenttags, tasklist_buttons)

    -- Create the wibox
    s.mywibox = awful.wibar({ position = wibar_pos, screen = s }) 
    mytray=wibox.widget.systray()
    mytray:set_screen(s)
    -- Add widgets to the wibox
    s.mywibox:setup {
        layout = wibox.layout.align.horizontal,
        { -- Left widgets
            layout = wibox.layout.fixed.horizontal,
            mylauncher,
            s.mytaglist,
            s.mypromptbox,
            separator,
        },
        s.mytasklist, -- Middle widget
        { -- Right widgets
            layout = wibox.layout.fixed.horizontal,
            mykeyboardlayout,
            separator,
            mytray,
            --wibox.widget.systray(),
            mytextclock,
            s.mylayoutbox,
        },
    }
end)
-- }}}

-- {{{ Mouse bindings
root.buttons(gears.table.join(
	awful.button({ }, 1, function () mymainmenu:hide() end),
    awful.button({ }, 3, function () mymainmenu:toggle() end),
    awful.button({ }, 4, awful.tag.viewnext),
    awful.button({ }, 5, awful.tag.viewprev)

))
-- }}}

-- {{{ Key bindings keybindings shortcuts
globalkeys = gears.table.join(
        -- Awesome
    awful.key({ modkey,           }, "s",      hotkeys_popup.show_help,
              {description="show help", group="awesome"}),
    awful.key({ modkey,           }, "f2", function () mymainmenu:show() end,
              {description = "show main menu", group = "awesome"}),
    awful.key({ modkey, "Control" }, "r", awesome.restart,
              {description = "reload awesome", group = "awesome"}),
    awful.key({ modkey, "Shift"   }, "q", awesome.quit,
              {description = "quit awesome", group = "awesome"}),
    awful.key({ "Control", altkey}, "Delete", function () awful.spawn(lock) end,
              {description = "lock computer", group = "awesome"}),
    awful.key({ "Control", "Shift", altkey}, "Delete", function () awful.spawn("xkill") end,
              {description = "spawn xkill", group = "awesome"}),
    
        -- Languages
    awful.key({ modkey, }, "F12", function () awful.util.spawn_with_shell( "$HOME/scripts/switch_us_layout" ) end,        
              {description = "Switch to US" , group = "language" }),
    awful.key({ modkey, }, "F11", function () awful.spawn("setxkbmap dvorak") end,        
              {description = "Switch to Dvorak" , group = "language" }),
    awful.key({ modkey, }, "F9", function () awful.spawn("setxkbmap jp") end,        --still needs heavy work
              {description = "Switch to JP" , group = "language" }),
    
        -- Tag
    --awful.key({ modkey,           }, "Left",   awful.tag.viewprev,
    --          {description = "view previous", group = "tag"}),
    --awful.key({ modkey,           }, "Right",  awful.tag.viewnext,
    --          {description = "view next", group = "tag"}),
    awful.key({ modkey,           }, "Escape", awful.tag.history.restore,
              {description = "go back", group = "tag"}),

        -- Client
    

        -- Layout manipulation
    awful.key({ modkey,           }, "d",function () awful.client.focus.byidx( 1)  end,
              {description = "focus next by index", group = "client"}),
    awful.key({ modkey,           }, "a",function () awful.client.focus.byidx(-1)  end,
              {description = "focus previous by index", group = "client"}),
    awful.key({ modkey,           }, "k",function () awful.client.focus.global_bydirection("up")  end,
              {description = "focus client upwards", group = "client"}),
    awful.key({ modkey,           }, "h",function () awful.client.focus.global_bydirection("left")  end,
              {description = "focus client to the left", group = "client"}),
    awful.key({ modkey,           }, "j",function () awful.client.focus.global_bydirection("down")  end,
              {description = "focus client downwards", group = "client"}),
    awful.key({ modkey,           }, "l",function () awful.client.focus.global_bydirection("right")  end,
              {description = "focus client to the right", group = "client"}),
    awful.key({ modkey, "Shift"   }, "space", function () awful.layout.inc(1)                       end,
              {description = "select next", group = "layout"}),
    awful.key({ modkey, "Control"   }, "space", function () awful.layout.inc(-1)                       end,
              {description = "select previous", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "j", function () awful.client.swap.byidx(  1)    end,
              {description = "swap with next client by index", group = "client"}),
    awful.key({ modkey, "Shift"   }, "d", function () awful.client.swap.byidx(  1)    end,
              {description = "swap with next client by index", group = "client"}),
    awful.key({ modkey, "Shift"   }, "k", function () awful.client.swap.byidx( -1)    end,
              {description = "swap with previous client by index", group = "client"}),
    awful.key({ modkey, "Shift"   }, "a", function () awful.client.swap.byidx( -1)    end,
              {description = "swap with previous client by index", group = "client"}),
    awful.key({ altkey  }, "Tab", function () awful.screen.focus_relative( 1) end,
              {description = "focus the other screen", group = "screen"}),
    awful.key({ modkey,           }, "u", awful.client.urgent.jumpto,
              {description = "jump to urgent client", group = "client"}),
    awful.key({                   }, "#193", awful.client.urgent.jumpto, --M3 key
              {description = "jump to urgent client (M3 key)", group = "client"}),
    awful.key({ modkey,           }, "Right",     function () awful.tag.incmwfact( 0.05)                 end,
              {description = "increase master width factor", group = "layout"}),
    awful.key({ modkey,           }, "Left",     function () awful.tag.incmwfact(-0.05)                 end,
              {description = "decrease master width factor", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "Left",     function () awful.tag.incnmaster( 1, nil, true)        end,
              {description = "increase the number of master clients", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "Right",     function () awful.tag.incnmaster(-1, nil, true)        end,
              {description = "decrease the number of master clients", group = "layout"}),
    awful.key({ modkey, "Control" }, "Left",     function () awful.tag.incncol( 1, nil, true)           end,
              {description = "increase the number of columns", group = "layout"}),
    awful.key({ modkey, "Control" }, "Right",     function () awful.tag.incncol(-1, nil, true)           end,
              {description = "decrease the number of columns", group = "layout"}),
    awful.key({ modkey,           }, "Down",     function () awful.tag.incmwfact( 0.05)                 end,
              {description = "increase master width factor", group = "layout"}),
    awful.key({ modkey,           }, "Up",     function () awful.tag.incmwfact(-0.05)                 end,
              {description = "decrease master width factor", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "Up",     function () awful.tag.incnmaster( 1, nil, true)        end,
              {description = "increase the number of master clients", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "Down",     function () awful.tag.incnmaster(-1, nil, true)        end,
              {description = "decrease the number of master clients", group = "layout"}),
    awful.key({ modkey, "Control" }, "Up",     function () awful.tag.incncol( 1, nil, true)           end,
              {description = "increase the number of columns", group = "layout"}),
    awful.key({ modkey, "Control" }, "Down",     function () awful.tag.incncol(-1, nil, true)           end,
              {description = "decrease the number of columns", group = "layout"}),
    awful.key({ modkey,           }, "Tab", function () awful.client.focus.history.previous()
                                                if client.focus then
                                                    client.focus:raise()
                                                end
                                            end,
                {description = "go back", group = "client"}),
    awful.key({ modkey,           }, "y",     function () if (awful.layout.get(s) ~= awful.layout.suit.floating) 
                                                            then awful.layout.set(awful.layout.suit.floating)
                                                            else awful.layout.set(awful.layout.suit.tile)  end
                                                                                              end,
              {description = "toggle floating|tiling", group = "layout"}),
    
    
    -- Music
    awful.key({                   }, "#195", function () awful.spawn(next_song) end,
              {description = "next song", group = "music"}),
    awful.key({                   }, "#191", function () awful.spawn(previous_song) end,
              {description = "previous song", group = "music"}),
    awful.key({                   }, "#192", function () awful.spawn(toggle_pause) end,
              {description = "play/pause", group = "music"}),

    
    -- Standard programs launcher 
    awful.key({ modkey,           }, "p", function () awful.util.spawn_with_shell("$HOME/.config/dmenu/scripts/edit-configs.sh") end,
              {description = "dmenu list of dotfiles", group = "launcher"}),
    awful.key({ modkey,           }, "#81", function () awful.spawn(calculator) end,
              {description = "open a calculator", group = "launcher"}),
    awful.key({ modkey,           }, "Return", function () awful.spawn(terminal_fish) end,
              {description = "open a terminal", group = "launcher"}),
    awful.key({ modkey,           }, "#104", function () awful.spawn(terminal_fish) end,
              {description = "open a terminal", group = "launcher"}),
    awful.key({ modkey,           }, "Insert", function () awful.spawn(music_player) end,
              {description = "open a music player", group = "launcher"}),
    awful.key({ modkey            }, "b", function () awful.spawn(browser)          end,
              {description = "launch Browser", group = "launcher"}),
    awful.key({ modkey,    "Shift"}, "b", function () awful.spawn(backup_browser)          end,
              {description = "launch backup Browser", group = "launcher"}),
    --[[awful.key({ modkey, "Control"}, "Escape", function () awful.spawn("/usr/bin/rofi -show drun -modi drun") end,
              {description = "launch rofi", group = "launcher"}),--]]
    awful.key({ modkey, "Shift"   }, "g", function () awful.spawn("steam")            end,
              {description = "launch steam", group = "launcher"}),
    awful.key({ modkey,           }, "g", function () awful.spawn("lutris")            end,
              {description = "launch lutris", group = "launcher"}),
    awful.key({ modkey,           }, "#86", function () awful.spawn("discord")            end,
              {description = "launch discord", group = "launcher"}),
    awful.key({ modkey, "Shift"   }, "z", function () awful.spawn("kate")            end,
              {description = "launch kate", group = "launcher"}),
   awful.key({ modkey,           }, "z", function () awful.spawn(editor)            end,
              {description = "launch neovim", group = "launcher"}),
   awful.key({ modkey,           }, "e", function () awful.spawn(filemanager)            end,
              {description = "launch file manager", group = "launcher"}),
   awful.key({ "Control", "Shift" }, "Escape", function () awful.spawn(task_manager)            end,
              {description = "launch task manager", group = "launcher"}),

    
        -- Screenshots
    awful.key({                   }, "Print", function () awful.spawn.with_shell("~/hidden_scripts/all_ss")   end,
              {description = "capture a screenshot", group = "screenshot"}),
    awful.key({"Control"          }, "Print", function () awful.spawn.with_shell("~/hidden_scripts/2_desktop_ss")   end,
              {description = "capture a screenshot of alt desktop", group = "screenshot"}),
    awful.key({"Shift"            }, "Print", function () awful.spawn.with_shell("~/hidden_scripts/1_desktop_ss")   end,
              {description = "capture a screenshot of main desktop", group = "screenshot"}),
    awful.key({ modkey            }, "v", function () awful.spawn.with_shell("~/hidden_scripts/region_ss")   end,
              {description = "capture a screenshot of selection", group = "screenshot"}),

    awful.key({ modkey, "Control" }, "n",
              function ()
                  local c = awful.client.restore()
                  -- Focus restored client
                  if c then
                      client.focus = c
                      c:raise()
                  end
              end,
              {description = "restore minimized", group = "client"}),

    -- Prompt
    awful.key({ modkey },            "F3",     function () awful.screen.focused().mypromptbox:run() end,
              {description = "run prompt", group = "launcher"}),

    awful.key({ modkey }, "ö",
              function ()
                  awful.prompt.run {
                    prompt       = "Run Lua code: ",
                    textbox      = awful.screen.focused().mypromptbox.widget,
                    exe_callback = awful.util.eval,
                    history_path = awful.util.get_cache_dir() .. "/history_eval"
                  }
              end,
              {description = "lua execute prompt", group = "awesome"}),
    -- Menubar
    awful.key({ modkey }, "r", function() menubar.show() end,
              {description = "show the menubar", group = "launcher"})
)

clientkeys = gears.table.join(
    awful.key({modkey,            }, "i",  function(c, s)
            local index=c.first_tag.index
            c:move_to_screen(s)
            local tag =c.screen.tags[index]
            c:move_to_tag(tag)
            if tag then
                    tag:view_only()
            end
        end,
              {description = "move to other screen on the same tag", group="client"}),
    awful.key({ modkey,           }, "f",
        function (c)
            c.fullscreen = not c.fullscreen
            c:raise()
        end,
        {description = "toggle fullscreen", group = "client"}),
    awful.key({ modkey,           }, "x",      function (c) c:kill()                         end,
              {description = "close", group = "client"}),
    awful.key({ modkey,  }, "space",  awful.client.floating.toggle                     ,
              {description = "toggle floating", group = "client"}),
    awful.key({ modkey, "Shift"   }, "Return", function (c) c:swap(awful.client.getmaster()) end,
              {description = "move to master", group = "client"}),
    awful.key({ modkey,           }, "o",      function (c) c:move_to_screen()               end,
              {description = "move to other screen", group = "client"}),
    awful.key({ modkey,           }, "q",      function (c) c:move_to_screen()               end,
              {description = "move to other screen", group = "client"}),
    awful.key({ modkey,           }, "t",      function (c) c.ontop = not c.ontop            end,
              {description = "toggle keep on top", group = "client"}),
    awful.key({ modkey,           }, "n",
        function (c)
            -- The client currently has the input focus, so it cannot be
            -- minimized, since minimized clients can't have the focus.
            c.minimized = true
        end ,
        {description = "minimize", group = "client"}),
    awful.key({ modkey,           }, "w",
        function (c)
            c.maximized = not c.maximized
            c:raise()
        end ,
        {description = "(un)maximize", group = "client"}),
    awful.key({ modkey, "Shift" }, "w",
        function (c)
            c.maximized_vertical = not c.maximized_vertical
            c:raise()
        end ,
        {description = "(un)maximize vertically", group = "client"}),
    awful.key({ modkey, "Control"   }, "w",
        function (c)
            c.maximized_horizontal = not c.maximized_horizontal
            c:raise()
        end ,
        {description = "(un)maximize horizontally", group = "client"})
)
        
for i = 1, 9 do
    -- Hack to only show tags 1 and 9 in the shortcut window (mod+s)
    local descr_view, descr_toggle, descr_move, descr_toggle_focus
    if i == 1 or i == 9 then
        descr_view = {description = "view tag #", group = "tag"}
        descr_toggle = {description = "toggle tag #", group = "tag"}
        descr_move = {description = "move focused client to tag #", group = "tag"}
        descr_toggle_focus = {description = "toggle focused client on tag #", group = "tag"}
    end
    globalkeys = my_table.join(globalkeys,
        -- View tag only.
        awful.key({ modkey }, "#" .. i + 9,
                  function ()
                        local screen = awful.screen.focused()
                        local tag = screen.tags[i]
                        if tag then
                           tag:view_only()
                        end
                  end,
                  descr_view),
        -- Toggle tag display.
        awful.key({ modkey, "Control" }, "#" .. i + 9,
                  function ()
                      local screen = awful.screen.focused()
                      local tag = screen.tags[i]
                      if tag then
                         awful.tag.viewtoggle(tag)
                      end
                  end,
                  descr_toggle),
        -- Move client to tag.
        awful.key({ modkey, "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:move_to_tag(tag)
                          end
                     end
                  end,
                  descr_move),
        -- Toggle tag on focused client.
        awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:toggle_tag(tag)
                          end
                      end
                  end,
                  descr_toggle_focus)
    )
end
   
clientbuttons = gears.table.join(
    awful.button({ }, 1, function (c) client.focus = c; c:raise()
                 mymainmenu:hide() end),
    awful.button({ modkey }, 1, awful.mouse.client.move),
    awful.button({        }, 2, function(c) client.focus = c; c:raise() end),
    awful.button({ modkey }, 3, awful.mouse.client.resize))

-- Set keys
root.keys(globalkeys)
-- }}}

-- {{{ Rules
-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
    -- All clients will match this rule.
    { rule = { },
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
                     focus = awful.client.focus.filter,
                     raise = true,
                     keys = clientkeys,
                     buttons = clientbuttons,
                     size_hints_honor = false, -- Remove gaps between terminals
                     screen = awful.screen.preferred,
                     callback = awful.client.setslave,
                     placement = awful.placement.no_overlap+awful.placement.no_offscreen
     }
    },
--floating clients
    { rule_any = {
        instance = {
          "DTA",  -- Firefox addon DownThemAll.
          "copyq",  -- Includes session name in class.
          "pinentry",
        },
        class = {
          "Arandr",
          "Blueman-manager",
          "Gpick",
          "Kruler",
          "MessageWin",  -- kalarm.
          "Sxiv",
          "Tor Browser", -- Needs a fixed window size to avoid fingerprinting by screen size.
          "Wpa_gui",
          "veromix",
          "kcalc",
          "xtightvncviewer"},

        -- Note that the name property shown in xprop might be set slightly after creation of the client
        -- and the name shown there might not match defined rules here.
        name = {
          "Event Tester",  -- xev.
		  "Search", --npqq search
          "galculator", 
        },
        role = {
          "AlarmWindow",  -- Thunderbird's calendar.
          "ConfigManager",  -- Thunderbird's about:config.
          "pop-up",       -- e.g. Google Chrome's (detached) Developer Tools.
          "setup",
        }
      }, properties = { floating = true }},

    -- Add titlebars to normal clients and dialogs
    { rule_any = {type = { "normal", "dialog" } },
      properties = { titlebars_enabled = false }},
	
    -- set client to appear on tag|screen
        { rule = { class = "Audacious" },
    	 properties = { screen = 2, tag = " MUS " } },
        { rule = { class = "mpv" },
    	 properties = {tag = " VID " } },
	   { rule = { class = "discord" },
    	 properties = { screen = 2, tag = " CHAT " } },
       { rule = { class = "sc2_x64" },
    	 properties = { screen = 1, tag = " GAM " } },
        { rule = { class = "pathofexile_x64steam" },
    	 properties = { screen = 1, tag = " GAM " } }, 
        { rule = { class = "firefox" },
    	 properties = {border_width=0} }, 
    -- Set Firefox to always map on the tag named "2" on screen 1.
    -- { rule = { class = "Firefox" },
    --   properties = { screen = 1, tag = "2" } },


}
    
-- }}}

-- {{{ Signals


-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c)
    -- Set the windows at the slave,
    -- i.e. put it at the end of others instead of setting it master.
    -- if not awesome.startup then awful.client.setslave(c) end

    if awesome.startup and
      not c.size_hints.user_position
      and not c.size_hints.program_position then
        -- Prevent clients from being unreachable after screen count changes.
        awful.placement.no_offscreen(c)
    end
end)

-- Add a titlebar if titlebars_enabled is set to true in the rules.
client.connect_signal("request::titlebars", function(c)
    -- buttons for the titlebar
    local buttons = gears.table.join(
        awful.button({ }, 1, function()
            client.focus = c
            c:raise()
            awful.mouse.client.move(c)
        end),
        awful.button({ }, 3, function()
            client.focus = c
            c:raise()
            awful.mouse.client.resize(c)
        end)
    )

    awful.titlebar(c) : setup {
        { -- Left
            awful.titlebar.widget.iconwidget(c),
            buttons = buttons,
            layout  = wibox.layout.fixed.horizontal
        },
        { -- Middle
            { -- Title
                align  = "center",
                widget = awful.titlebar.widget.titlewidget(c)
            },
            buttons = buttons,
            layout  = wibox.layout.flex.horizontal
        },
        { -- Right
            awful.titlebar.widget.floatingbutton (c),
            awful.titlebar.widget.stickybutton   (c),
            awful.titlebar.widget.ontopbutton    (c),
            awful.titlebar.widget.maximizedbutton(c),
            awful.titlebar.widget.closebutton    (c),
            layout = wibox.layout.fixed.horizontal()
        },
        layout = wibox.layout.align.horizontal
    }

end)

client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)

-- Disable borders on lone windows
-- Handle border sizes of clients.
for s = 1, screen.count() do screen[s]:connect_signal("arrange", function ()
  local clients = awful.client.visible(s)
  local layout = awful.layout.getname(awful.layout.get(s))

  for _, c in pairs(clients) do
    -- No borders with only one humanly visible client
    if c.maximized then
      -- NOTE: also handled in focus, but that does not cover maximizing from a
      -- tiled state (when the client had focus).
      c.border_width = 0
    elseif c.floating or layout == "floating" then
      c.border_width = beautiful.border_width
    elseif layout == "max" or layout == "fullscreen" then
      c.border_width = 0
    else
      local tiled = awful.client.tiled(c.screen)
      if #tiled == 1 then -- and c == tiled[1] then
        tiled[1].border_width = 0
        -- if layout ~= "max" and layout ~= "fullscreen" then
        -- XXX: SLOW!
        -- awful.client.moveresize(0, 0, 2, 0, tiled[1])
        -- end
      else
        c.border_width = beautiful.border_width
      end
    end
  end
end)
end
    -- Notification Rules {{{
ruled.notification.connect_signal('request::rules', function()
    -- Add a red background for urgent notifications.
--[[
    ruled.notification.append_rule {
        rule       = { urgency = 'critical' },
        properties = { bg = '#ff0000', fg = '#ffffff', timeout = 0 }
    }
--]]

    -- Or green background for normal ones.
    -- notification and max height/width of normal notifications
    ruled.notification.append_rule {
        rule       = { urgency = 'normal' },
        properties = { --bg      = '#00ff00', 
                        --fg = '#000000',
                        screen = 2,
                        max_height = 150,
                        max_width = 500}
    }
end)
--[[
-- Create a normal notification.
naughty.notification {
    title   = 'A notification 1',
    message = 'This is very informative',
    icon    = beautiful.awesome_icon,
    urgency = 'normal',
}

-- Create a normal notification.
naughty.notification {
    title   = 'A notification 2',
    message = 'This is very informative',
    icon    = beautiful.awesome_icon,
    urgency = 'critical',
}
--}}}
--]]


--show titlebars only on floating windows
client.connect_signal("property::floating", function(c)
    if (c.floating == true) and (c.maximized_vertical == false) and (c.maximized == false) and (c.fullscreen == false) then
        awful.titlebar.show(c)
    else
        awful.titlebar.hide(c)
    end
end)
-- }}}



awful.spawn.with_shell("~/.config/awesome/autorun.sh")

