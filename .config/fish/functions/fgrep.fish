# Defined in - @ line 1
function fgrep --wraps='fgrep -i --colour=auto' --description 'alias fgrep=fgrep -i --colour=auto'
 command fgrep -i --colour=auto $argv;
end
