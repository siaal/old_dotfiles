# Defined in - @ line 1
function l --wraps='exa --color=always --group-directories-first' --description 'alias l=exa --color=always --group-directories-first'
  exa --color=always --group-directories-first $argv;
end
