# Defined in - @ line 1
function backup --wraps='cp -i .bak' --description 'alias backup=cp -i .bak'
  cp -i $argv $argv.bak;
end
