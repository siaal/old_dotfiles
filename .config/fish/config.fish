function fish_greeting; end # remove fish greeting

fish_vi_key_bindings               #vim keybinds
set -x PATH "/home/andrew/scripts:/home/andrew/.config/lynx:/home/andrew/.local/bin:/usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/lib/jvm/default/bin:/usr/bin/site_perl:/usr/bin/vendor_perl:/usr/bin/core_perl:/var/lib/snapd/snap/bin"
set -x CDPATH "./:/home/andrew/Repos/gitlab.com/siaal:/home/andrew/Repos/github.com/siaal:/home/andrew/Repos/gitlab.com/*:/home/andrew/Repos/github.com/*:/home/andrew/Repos/gitlab.com:/home/andrew/Repos/github.com:/home/andrew/Repos/:/home/andrew"
# set -x CDPATH "./ /home/andrew/Repos/gitlab.com/siaal /home/andrew/Repos/github.com/siaal /home/andrew/Repos/gitlab.com/* /home/andrew/Repos/github.com/* /home/andrew/Repos/gitlab.com /home/andrew/Repos/github.com /home/andrew/Repos/ /media/andrew /home/andrew"

#set -x PAGER "most"                #try and get some colour into most

# Colored man pages
set -x LESS_TERMCAP_mb $magen
set -x LESS_TERMCAP_md $yellow
set -x LESS_TERMCAP_me $reset
set -x LESS_TERMCAP_se $reset
set -x LESS_TERMCAP_so $blue
set -x LESS_TERMCAP_ue $reset
set -x LESS_TERMCAP_us $violet
