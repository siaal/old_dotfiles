# rebind kj to switch to normal mode in VI-mode-fish
bind -M insert -m default kj force-repaint
bind -M replace -m default kj force-repaint


# original credit to bang-bang fish addon which regretably does not work with vi-mode
bind -M insert ! __history_previous_command
bind -M insert '$' __history_previous_command_arguments
