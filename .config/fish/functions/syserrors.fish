# Defined in - @ line 1
function syserrors --wraps='sudo journalctl -p 3 -xb' --description 'alias syserrors=sudo journalctl -p 3 -xb'
  sudo journalctl -p 3 -xb $argv;
end
